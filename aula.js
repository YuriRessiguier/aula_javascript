//Yuri Nogueira Ressiguier
//Aula JavaScript 29/07


var texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. Phasellus non dictum eros. Praesent cursus laoreet ipsum, in porta nisi hendrerit eu. Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod. Curabitur quis neque in magna efficitur luctus mollis vel odio. In eu condimentum orci. Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus. Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus. Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus. In sit amet porta turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus. Integer hendrerit tortor id pharetra ultrices. Suspendisse cursus suscipit congue. Vestibulum ornare faucibus interdum. Aliquam dapibus elit sed lorem laoreet tincidunt. Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit."

var textoNum = ""

//Percorre toda String e verifica todas as vogais [transformadas em minúsculas] e converte em um inteiro
for (let i = 0; i <= texto.length - 1; i++) {
    switch (texto[i].toLowerCase()){
        case ".":
            textoNum += "0";
            break;
        case ",":
            textoNum += "-1";
            break;
        case "a":
            textoNum += "1";
            break;
        case "b":
            textoNum += "2";
            break;
        case "c":
            textoNum += "3";
            break;
        case "d":
            textoNum += "4";
            break;
        case "e":
            textoNum += "5";
            break;
        case "f":
            textoNum += "6";
            break;
        case "g":
            textoNum += "7";
            break;
        case "h":
            textoNum += "8";
            break;
        case "i":
            textoNum += "9";
            break;
        case "j":
            textoNum += "10";
            break;
        case "k":
            textoNum += "11";
            break;
        case "l":
            textoNum += "12";
            break;
        case "m":
            textoNum += "13";
            break;
        case "n":
            textoNum += "14";
            break;
        case "o":
            textoNum += "15";
            break;
        case "p":
            textoNum += "16";
            break;
        case "q":
            textoNum += "17";
            break;
        case "r":
            textoNum += "18";
            break;
        case "s":
            textoNum += "19";
            break;
        case "t":
            textoNum += "20";
            break;
        case "u":
            textoNum += "21";
            break;
        case "v":
            textoNum += "22";
            break;
        case "w":
            textoNum += "23";
            break;
        case "x":
            textoNum += "24";
            break;
        case "y":
            textoNum += "25";
            break;
        case "z":
            textoNum += "26";
            break;
    }
}
//Printa Resultado
console.log(textoNum)